import {computed, defineComponent, inject, reactive, ref, watch} from "vue";
import {mapActions, useStore} from "vuex";
import {MetaConst} from "@/utils/MetaUtils";
import CoreConsts from "@/components/CoreConsts";
import {FormContext} from "@/components/form/basic/FormContext";

export default defineComponent({
    props: {
        url: String,
        dict: String,
        options: Array,
        labelField: {default: CoreConsts.Options_LabelField},
        valueField: {default: CoreConsts.Options_ValueField},
    },
    data() {
        return {
            dataSource: null
        }
    },
    created() {
        if(!this.options) {
            let valueField = this.$attrs.fieldNames ? this.$attrs.fieldNames.value || this.$attrs.fieldNames.key : this.valueField;
            let labelField = this.$attrs.fieldNames ? this.$attrs.fieldNames.label || this.$attrs.fieldNames.title : this.labelField;

            if(this.dict) {
                this.dataSource = ref(useStore().getters['sys/getOptionsByDictType'](this.dict, labelField, valueField));
            } else if(this.url) {// 实现动态级联获取 /test?type={typeId}
                let url = this.url;
                let fields = this.url.match(/\{(.*)\}/g);
                if(fields instanceof Array) {
                    let editModel = ref({})
                    let formContext = inject("formContext");
                    if(formContext instanceof FormContext) {
                        editModel = formContext.getEditModel();
                    }

                    fields.forEach(field => {
                        let fieldStr = field.substr(1, field.length - 2);
                        let fieldValue = editModel[fieldStr] == null ? '' : editModel[fieldStr];
                        url = url.replace(field, fieldValue);
                    })

                    let store = useStore();
                    // 监控表单数据对象变化, 校验是否需要重新获取数据
                    watch(formContext.getEditModel, (newVal, oldVal) => {
                        if(typeof newVal == 'object') {
                            // 根据最新的model获取最新的url
                            let newUrl = this.getUrl(fields, newVal);
                            if(url != newUrl) { // url地址不一样, 说明需要重新获取数据
                                this.dataSource = ref(store.getters['sys/getOptionsByUrl'](url = newUrl, labelField, valueField));
                                // 只对同一条记录切换的时候才重置值
                                if(newVal == oldVal) {
                                    // 重置当前表单的数据
                                    formContext.setFieldValue(this.namePath, null)
                                }
                            }
                        }
                    }, {deep: true})
                }

                this.dataSource = ref(useStore().getters['sys/getOptionsByUrl'](url, labelField, valueField));
            }
        } else {
            this.dataSource = this.options;
        }
    },
    methods: {
        getUrl(fields, editModel) {
            let url = this.url;
            fields.forEach(field => {
                let fieldStr = field.substr(1, field.length - 2);
                let fieldValue = editModel[fieldStr] == null ? '' : editModel[fieldStr];
                url = url.replace(field, fieldValue);
            })

            return url;
        }
    }
})

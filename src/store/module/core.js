/*系统模块相关配置*/
import {getDict, getMenus, getUser} from "@/api";
import {reactive, ref} from "vue";
import {GET} from "@/utils/request";
import router, {resolverMenuToRoutes} from "@/router";
import CoreConsts from "@/components/CoreConsts";

// 解析菜单映射
function resolverMenuMaps(menus) {
    let urlMenuMap = {}, idMenuMap = {}, authMenuMap = {};
    let doResolverMenuMaps = (menus) => {
        for(let i=0; i < menus.length; i++) {
            let menu = menus[i];
            idMenuMap[menu.id] = menu;
            if (menu['type'] === 'V') { // 视图类型
                urlMenuMap[menu.url] = menu;
            } else if(menu['type'] === 'A') { // 权限类型
                if(menu['perms']) { // 通过权限字符串校验权限
                    authMenuMap[menu['perms']] = menu;
                }

                if(menu.url) { // 通过url校验权限
                    authMenuMap[menu.url] = menu;
                }
            }

            if (menu['children']) {
                doResolverMenuMaps(menu['children'])
            }
        }
    }

    doResolverMenuMaps(menus)
    return {urlMenuMap, idMenuMap, authMenuMap};
}

function resolverBreadcrumbRoutes(menus, routes, child) {
    menus.forEach(menu => {

    })
}

function resolverOptions(data, labelField, valueField, valueLabelMap) {
    data.forEach(item => {
        item['label'] = item[labelField];
        item['value'] = item[valueField];
        valueLabelMap[item['value']] = item['label'];

        if(item.children instanceof Array) {
            resolverOptions(item.children, labelField, valueField, valueLabelMap);
        }
    })
}


const registerSysModule = function (store) {
    store.registerModule('sys', {
        namespaced: true,
        state: {
            user: {}, // 当前登录的用户
            views: [], // 视图信息
            init: false, // 系统路由是否已经初始化(菜单初始化, 用于动态路由404问题)
            userKey: null,
            collapsed: false,
            theme: 'default', // 默认主题
            userVisible: false, // 用户中心

            openKeys: [], // 当前展开的子菜单key
            activeMenu: {}, // 当前激活的菜单
            activeRoute: {}, // 当前激活的路由
            activeView: {}, // 当前激活的视图
            taskBarData: [], // 任务栏打开的菜单
            optionsMaps: {}, // url | dict -> {options, valueLabelMap}
            idMenuMaps: {}, // id和菜单的映射
            selectedKeys: [], // 选中的菜单
            authMenuMap: {}, // perms -> menu 权限
            urlMenuMaps: {}, // url -> menu 对象
            urlRouteMaps: {}, // url -> route taskBarData
            optionsInfo: {/*dict -> data | url -> data*/}, // 字典和url的数据信息
            breadcrumbRoutes: [
                {path: '/', name: '首页', index: 0},
            ], // 面包屑路由信息
        },
        getters: {
            // 标识菜单是否已经初始化完成
            init: state => state.init,
            user: state => state.user,
            views: state => state.views,
            theme: state => state.theme,
            userKey: state => state.userKey,
            collapsed: state => state.collapsed,
            userVisible: state => state.userVisible,

            // 打开的子菜单
            openKeys: state => state.openKeys,

            // url -> menu 映射
            urlMenuMaps: state => state.urlMenuMaps,
            // id -> menu
            idMenuMaps: state => state.idMenuMaps,
            // perms -> menu 映射
            authMenuMap: state => state.authMenuMap,
            // 当前激活的视图
            activityView: state => state.activeView,
            // 当前激活的菜单
            activityMenu: state => state.activeMenu,
            // 当前激活的路由
            activeRoute: state => state.activeRoute,
            // 任务栏打开的任务列表
            taskBarData: state => state.taskBarData,
            // 选中的菜单
            selectedKeys: state => state.selectedKeys,
            //系统菜单
            menus: state => state.views,

            // 解析面包屑路径
            breadcrumbRoutes: state => {
                let activeMenu = state.activeMenu;
                let activeRoute = state.activeRoute;
                if(activeMenu && activeMenu.id) {
                    let idMenuMap = state.idMenuMaps[activeMenu.pid];
                    state.breadcrumbRoutes[1] = idMenuMap;
                    state.breadcrumbRoutes[2] = {path: activeRoute.path, name: activeRoute.name, index: 2}
                } else if(activeRoute.path != state.breadcrumbRoutes[0].path){
                    state.breadcrumbRoutes.splice(2, 1);
                    state.breadcrumbRoutes[1] = {path: activeRoute.path, name: activeRoute.name, index: 1}
                }

                return state.breadcrumbRoutes;
            },

            getOptionsByUrl: state => {
                return (url, labelField, valueField) => {
                    let options = state.optionsMaps[url];
                    if(!options) {
                        let dictData = reactive([]), valueLabelMap = reactive({});
                        state.optionsMaps[url] = {options: dictData, valueLabelMap};

                        GET(url).then(({data}) => {
                            if(data instanceof Array) {
                                resolverOptions(data, labelField, valueField, valueLabelMap)

                                dictData.push(...data)
                            }
                        })
                    }
                    return state.optionsMaps[url].options
                }
            },

            getValueLabelMap: state => {
              return key =>  state.optionsMaps[key].valueLabelMap;
            },

            getOptionsByDictType: (state) => {
                return (dictType, labelField, valueField) => {
                    let dictData = reactive([]), valueLabelMap = reactive({});
                    let options = state.optionsMaps[dictType];
                    if(!options) { // 说明字典数据还不存在, 先缓存
                        state.optionsMaps[dictType] = {options: dictData, valueLabelMap};

                        getDict(dictType).then((options) => {
                            if(options instanceof Array) {
                                options.forEach(item => {
                                    item['label'] = item[labelField];
                                    item['value'] = item[valueField];
                                })

                                dictData.push(...options)
                                options.forEach(item => {
                                    valueLabelMap[item.value] = item.label;
                                })
                            }
                        })
                    }

                    return state.optionsMaps[dictType].options
                }
            }
        },
        mutations: {
            // 切换主题
            switchTheme(state, theme) {
                state.theme = theme;
            },
            // 任务栏切换
            pushAndSwitchTask: (state, url) => {
                let route = state.urlRouteMaps[url];
                if(route == null) {
                    return console.error(`url[${url}]对应的路由不存在`)
                }

                router.push(route).then(() => {
                    store.commit('sys/switchActiveMenuTo', url)
                }).catch(reason=> {
                    console.error(`切换任务失败(组件异常或组件不存在或未注册路由)[${reason}]`)
                });
            },

            // 移除任务栏的任务
            removeTask: (state, url) => {
                let urlRoute = state.urlRouteMaps[url];
                if(urlRoute) {
                    state.urlRouteMaps[url] = null;
                    let number = state.taskBarData.findIndex(route => route.path == url);
                    if(number > -1) {
                        state.taskBarData.splice(number, 1);
                    }
                }
            },
            // 在任务栏上面打开一个任务
            openUrlOrSwitchTask: (state, url) => {
                router.push(url).catch(reason=> {
                    console.error(`打开菜单失败(组件不存在或者未注册路由)`)
                });
            },
            openOrSwitchTask: (state, route) => {
                let path = route.path;
                // 首页(工作台)作为第一个任务
                if(state.taskBarData.length == 0 && route.path != '/') {
                    return router.push('/').then(() => {
                        router.push(route).finally();
                    })
                }

                if(!state.urlRouteMaps[path]) {
                    state.taskBarData.push(route);
                }

                state.activeRoute = route;
                state.urlRouteMaps[path] = route;
            },
            setRouteKeepAlive: (state, {url, componentName}) => {
                let urlRoute = state.urlRouteMaps[url];
                if(urlRoute && componentName) {
                    urlRoute.meta['keepAlive'] = componentName;
                }
            },
            switchActiveRouteTo: (state, route) => {
                state.activeRoute = route;
            },
            // 切换激活的菜单
            switchActiveMenuTo: (state, url) => {
                let menu = state.urlMenuMaps[url];

                if(menu) {
                    state.activeMenu = menu;

                    state.selectedKeys.length = 0;
                    state.selectedKeys.push(menu.url);
                } else {
                    state.activeMenu = null;
                    state.selectedKeys.length = 0;
                }
            },

            // 在任务栏上面打开一个任务, 并展开此任务菜单的父菜单
            openUrlAndParentMenu: (state, url) => {
                let menu = state.urlMenuMaps[url];
                if(menu) {
                    store.commit('sys/openUrlOrSwitchTask', url);

                    // 打开菜单栏中此url对应的父菜单
                    let parent = state.idMenuMaps[menu.pid];
                    if(parent) {
                        store.commit('sys/switchOpenSubMenuTo', [parent.id]);
                    }
                }
            },

            // 往任务栏中增加新的任务
            addNewMenu: (state, menu) => {
                let taskUrl = menu['url'] != null ? menu['url'] : menu['path'];
                if(taskUrl == null) {
                    return console.warn(`菜单[${menu.name}]未指定url`)
                }

                if(!menu['name'] && import.meta.env.DEV) {
                    console.warn(`未指定任务name[${menu}]`)
                }

                let taskMenu = state.urlMenuMaps[taskUrl];
                if(!taskMenu) {
                    // 是否是侧边栏菜单
                    menu['isMenu'] = menu['isMenu'] || false;

                    state.urlMenuMaps[taskUrl] = menu;
                } else {
                    if(taskMenu.name == menu.name) {
                        console.warn(`此任务名[${menu.name}]已经存在, 任务[${taskMenu}]`)
                    }
                }
            },

            putMenuToTaskBars: (state, menu) => {
                state.taskBarData.push(menu)
            },

            switchActiveViewTo: (state, view) => {
                state.activeView = view;
            },
            switchOpenSubMenuTo: (state, openKeys) => {
                state.openKeys = openKeys
            },
            toggleUserVisible: (state, {visible, key}) => {
                state.userKey = key || state.userKey;
                state.userVisible = visible != null
                    ? visible : state.userVisible;
            },
            /**
             * 注销
             * 清除数据
             * @param state
             */
            logout: (state) => {
                state.init = false;
                state.openKeys = [];
                state.urlMenuMaps = {};
                state.taskBarData = [];
                state.urlRouteMaps = {};
                state.selectedKeys = [];
                state.activeRoute = null;
                state.userVisible = false;
            }
        },
        actions: {
            initUser: ({commit, state}) => {
                return getUser().then((resp) => {
                    if(resp) {
                        let {data, code, message} = resp;
                        if(code == CoreConsts.SuccessCode) {
                            state.user = data;
                        } else {
                            console.error(message || "获取用户失败");
                        }
                    }
                    return {};
                });
            },
            initMenus: ({commit, state}) => {
                 return getMenus().then((resp) => {
                     if(resp) {
                         let {data} = resp;
                         state.views = data;
                         // state.activeView = state.views[0];
                         let {urlMenuMap, idMenuMap, authMenuMap} = resolverMenuMaps(data);
                         state.idMenuMaps = idMenuMap;
                         state.authMenuMap = authMenuMap;

                         // 加入到菜单列表
                         if(urlMenuMap != null) {
                             Object.keys(urlMenuMap).forEach(key => {
                                 state.urlMenuMaps[key] = urlMenuMap[key];
                             })
                         }

                         resolverMenuToRoutes(urlMenuMap);
                         state.init = true; // 声明路由信息已经初始化完成
                         return urlMenuMap;
                     }

                    return {};
                });
            },
        }
    })
}

export default registerSysModule;
